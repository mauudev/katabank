import pytest
import sys

sys.path.insert(0, 'C:\\Users\\mauricio trigo\\Desktop\\KataBank\\src')
from Screen import Screen
from Number import Number

s1 = Screen()

sstr0 = """
    _  _  _  _  _     _  _
|_| _||_  _||_| _|  |  ||_|
  | _||_||_ |_||_   |  | _|
"""

sstr1 = """
       _  _  _        _  _
  ||_||_  _|| ||_|  || ||_|
  |  ||_| _||_|  |  ||_| _|
"""

sstr2 = """
 _  _  _  _  _  _     _   
|_| _||_  _||_| _|  |  ||_|
|_| _||_||_ |_||_   |  | _|
"""

sstr3 = """
    _  _  _  _  _  _  _  _
|_||_   ||_ | ||_|| || || |
  | _|  | _||_||_||_||_||_|
"""

sstr4 = """
 _  _  _  _  _  _     _   
 _| _ |_  _| _| _|     ||_|
|_  _||_ |_ |_||_   |  | _|
"""

def fillNumbers():
  n1 = Number("1")
  n2 = Number("2")
  n3 = Number("3")
  n4 = Number("4")
  n5 = Number("5")
  n6 = Number("6")
  n7 = Number("7")
  n8 = Number("8")
  n9 = Number("9")

  s1.addNumber(n1)
  s1.addNumber(n2)
  s1.addNumber(n3)
  s1.addNumber(n4)
  s1.addNumber(n5)
  s1.addNumber(n6)
  s1.addNumber(n7)
  s1.addNumber(n8)
  s1.addNumber(n9)

def clearScreen():
  s1.clearScreen()

def test_it_should_add_numbers():
  fillNumbers()
  assert len(s1.getNumbers()) == 9, "Test failed"

def test_it_should_append_an_empty_cell_number():
  sstr = "    _  _  _  _  _  _  _  _"
  sList = []
  s1.appendEmptyCell(sstr, sList)
  assert len(sList) == 1 and len(sList[0]) == 3, "Test failed"

def test_it_should_fill_screen_of_numbers():
  s1.fillScreen()
  assert len(s1.getScreenCells()) > 0, "Test failed"

def test_it_should_clear_screen_of_numbers():
  clearScreen()
  assert len(s1.getScreenCells()) == 0, "Test failed"

# TEST USE CASES
def test_it_should_return_read_numbers_case0():
  s1.fillScreenByStr(sstr0)
  s1.printNumbers()
  numbers = s1.readScreenNumbers()
  assert numbers == "4 3 6 2 8 2 1 7 9 ERR", "Test failed"

def test_it_should_return_read_numbers_case1():
  s1.fillScreenByStr(sstr1)
  s1.printNumbers()
  numbers = s1.readScreenNumbers()
  assert numbers == "1 4 6 3 0 4 1 0 9 ERR", "Test failed"

def test_it_should_return_read_numbers_case2():
  s1.fillScreenByStr(sstr2)
  s1.printNumbers()
  numbers = s1.readScreenNumbers()
  assert numbers == "8 3 6 2 8 2 1 7 ? ILL", "Test failed"

def test_it_should_return_read_numbers_case3():
  s1.fillScreenByStr(sstr3)
  s1.printNumbers()
  numbers = s1.readScreenNumbers()
  assert numbers == "4 5 7 5 0 8 0 0 0 ", "Test failed"

def test_it_should_return_read_numbers_case4():
  s1.fillScreenByStr(sstr4)
  s1.printNumbers()
  numbers = s1.readScreenNumbers()
  assert numbers == "2 ? ? 2 ? 2 ? 7 ? ILL", "Test failed"
# TEST USE CASES

def test_it_should_return_an_associated_numbers_list():
  fillNumbers()
  elements = s1.getElementsToValidate()
  elements_to_compare = { 'd1': 9, 'd2': 8, 'd3': 7, 'd4': 6, 'd5': 5, 'd6': 4, 'd7': 3, 'd8': 2, 'd9': 1 }
  assert elements == elements_to_compare, "Test failed"

def test_it_should_return_remainder_value_based_on_numbers_list():
  elements = { 'd1': 9, 'd2': 8, 'd3': 7, 'd4': 6, 'd5': 5, 'd6': 4, 'd7': 3, 'd8': 2, 'd9': 1 }
  remainder = s1.computeRemainder(elements)
  assert remainder == 165, "Test failed"

def test_it_should_validate_checksum():
  # (165) % 11 == 0 => False
  assert s1.validateChecksum() == True, "Test failed"
