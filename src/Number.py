from NumberDictionary import NumberDictionary

class Number:
  def __init__(self, number):
    self.number = (number, NumberDictionary.getLinesByNumber(number))
    
  def getNumber(self):
    return self.number[0]
  
  def getNumberLinesArray(self):
    return self.number[1]

  def numberDigitalForm(self):
    numberStr = ""
    i = 0
    linesArray = self.number[1]
    while i < len(linesArray):
      if i == 0:
        pass
      else:
        if i % 3 == 0:
          numberStr += '\n'
      numberStr += linesArray[i]
      i += 1
    return numberStr

  def numberStringForm(self):
    return NumberDictionary.numberToString(self.getNumber())
