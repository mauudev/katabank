import numpy as np
from LinesInterpreter import LinesInterpreter

class Screen:
  def __init__(self):
    self.numbersArray = []
    self.screen = []

  def addNumber(self, numberLines):
    self.numbersArray.append(numberLines)

  def concatRowValues(self, ssArray, rowIndex):
    res = ""
    lim = len(ssArray)
    i, j, index = rowIndex, 0, 0
    while index < lim:
      if j == 2:
        res += ssArray[index][i][j]
        j = 0
        index += 1
      else: 
        res += ssArray[index][i][j]
        j += 1
    return res

  def appendEmptyCell(self, sstr, mainList):
    display = np.full((3,3), '*', dtype='U1')
    for c, x in zip(sstr, np.nditer(display, op_flags=['readwrite'])):
      x[...] = c
    mainList.append(display)

  def fillScreen(self, sstr = None):
    if sstr == None:
      for number in self.numbersArray:
        sstr = number.numberStringForm()
        self.appendEmptyCell(sstr, self.screen)
    else:
        self.appendEmptyCell(sstr, self.screen)
      
  def printNumbers(self):
    res = ""
    k = 0
    while k < 3:
      res += self.concatRowValues(self.screen, k)
      res += '\n'
      k += 1
    return res

  def readScreenNumbers(self):
    numbers = LinesInterpreter.readNumbersByScreen(self.screen)
    self.numbersArray = numbers
    sstr = ""
    isIll = False
    checkSumValid = self.validateChecksum()
    for number in numbers:
      if number == "?":
        sstr += "? "
        isIll = True
      else:
        sstr += str(number.getNumber()) + " "
    if isIll:
      sstr += "ILL"
    if not checkSumValid and "ILL" not in sstr: 
      sstr += "ERR"
    self.clearScreen()
    return sstr
  
  def clearScreen(self):
    self.numbersArray.clear()
    self.screen.clear()

  def getNumbers(self):
    numbersArray = []
    i = 0
    lim = len(self.numbersArray)
    while i < lim and "?" not in self.numbersArray:
      currentNumber = self.numbersArray[i]
      numberValue = currentNumber.getNumber()
      numbersArray.append(int(numberValue))
      i  += 1
    return numbersArray

  def getScreenCells(self):
    return self.screen

  def getElementsToValidate(self):
    unorderedNumbers = self.getNumbers()
    numbers = unorderedNumbers[::-1]
    elements = {}
    lim, i, n = len(numbers), 0, 1
    while i < lim:
      value = numbers[i]
      key = "d" + str(n)
      elements[key] = value
      i += 1
      n += 1
    return elements

  # número de cuenta: 3 4 5 8 8 2 8 6 5
  # posiciones:   d9 d8 d7 d6 d5 d4 d3 d2 d1  
  #
  # El checksum se calcula con la fórmula:
  # (1*d1+2*d2+3*d3+4*d4+5*d5+6*d6+7*d7+8*d8+9*d9) mod 11 = 0

  def computeRemainder(self, elementsArray):
    remainder = 0
    lim = len(elementsArray)
    i, j, k = 0, 1, 1
    while i < lim:
      element = elementsArray.get('d' + str(j))
      if k == 9:
        remainder += k * elementsArray.get('d' + str(k))
        return remainder
      else:
        remainder += k * element
        i += 1
        j += 1
        k += 1
    return remainder
  
  def validateChecksum(self):
    elements = self.getElementsToValidate()
    size = len(elements)
    if size == 0: 
      return False
    else:
      remainder = self.computeRemainder(elements)
      return remainder % 11 == 0
  
  def fixSplitLinesList(self, inputStr):
    lines = inputStr.splitlines()
    sstr, uStr1, uStr2 = "", " ", lines[1]
    lines[1] = uStr2 + uStr1
    del lines[0]
    for sstrElem in lines:
      sstr += sstrElem
    return sstr

  def buildEmptyScreen(self):
    i = 0
    sstr = '*'
    while i < 9:
      self.appendEmptyCell(sstr, self.screen)
      i += 1

  def fillScreenByStr(self, sstr):
    self.buildEmptyScreen()
    sstrFixed = self.fixSplitLinesList(sstr)
    i, j, screenIndex, k  = 0, 0, 0, 0
    sstrFixedLim = len(sstrFixed)
    screenLim = len(self.screen)
    while k < sstrFixedLim:
      while screenIndex < screenLim:
        if j == 2:
          self.screen[screenIndex][i][j] = sstrFixed[k]
          j = 0
          k += 1
          screenIndex += 1
        else:
          self.screen[screenIndex][i][j] = sstrFixed[k]
          j += 1
          k += 1
      screenIndex = 0
      i += 1
      j = 0
