from collections import OrderedDict 

class NumberDictionary:
  numberDictionary = OrderedDict({ 
    "0": [ ' ','_',' ','|',' ','|','|','_','|' ],
    "1": [ ' ',' ',' ',' ',' ','|',' ',' ','|' ],
    "2": [ ' ','_',' ',' ','_','|','|','_',' ' ],
    "3": [ ' ','_',' ',' ','_','|',' ','_','|' ],
    "4": [ ' ',' ',' ','|','_','|',' ',' ','|' ],
    "5": [ ' ','_',' ','|','_',' ',' ','_','|' ],
    "6": [ ' ','_',' ','|','_',' ','|','_','|' ],
    "7": [ ' ','_',' ',' ',' ','|',' ',' ','|' ],
    "8": [ ' ','_',' ','|','_','|','|','_','|' ],
    "9": [ ' ','_',' ','|','_','|',' ','_','|' ]
  })
  
  @classmethod
  def getLinesByNumber(self, number):
    return self.numberDictionary.get(number)
  
  @classmethod
  def getNumberByLines(self, searchLines):
    for number, lines in self.numberDictionary.items():
      if lines == searchLines:
        return number
    return None

  @classmethod
  def countVerticalLines(self, number):
    charList = self.numberDictionary.get(number)
    verticalLines = 0
    if charList != None:
      for char in charList:
        if char == '|':
          verticalLines += 1
    return verticalLines

  @classmethod
  def countHorizontalLines(self, number):
    charList = self.numberDictionary.get(number)
    horizontalLines = 0
    if charList != None:
      for char in charList:
        if char == '_':
          horizontalLines += 1
    return horizontalLines

  @classmethod
  def countBlankSpaces(self, number):
    blankSpaces = (self.countHorizontalLines(number) + self.countVerticalLines(number)) - 9
    return blankSpaces

  @classmethod
  def numberToString(self, number):
    numberLinesList = self.numberDictionary.get(number)
    numberStr = ""
    if number != None:
      i, lim = 0, len(numberLinesList)
      while i < lim:
        numberStr += numberLinesList[i]
        i += 1
    return numberStr
