from Screen import Screen
# from Number import Number

# 3 4 5 8 8 2 8 6 5
# 4 5 7 5 0 8 0 0 0

# n0 = Number("8")
# n1 = Number("3")
# n2 = Number("6")
# n3 = Number("2")
# n4 = Number("8")
# n5 = Number("2")
# n6 = Number("1")
# n7 = Number("7")
# n8 = Number("9")

s1 = Screen()

# s1.addNumber(n0)
# s1.addNumber(n1)
# s1.addNumber(n2)
# s1.addNumber(n3)
# s1.addNumber(n4)
# s1.addNumber(n5)
# s1.addNumber(n6)
# s1.addNumber(n7)
# s1.addNumber(n8)
# #1 s.fillScreen()


sstr0 = """
    _  _  _  _  _     _  _
|_| _||_  _||_| _|  |  ||_|
  | _||_||_ |_||_   |  | _|
"""

sstr1 = """
       _     _        _   
  ||_||_ |_|| ||_|  || ||_|
  |  ||_|  ||_|  |  ||_|  |
"""

sstr2 = """
 _  _  _  _  _  _     _  _
|_| _||_  _||_| _|  |  ||_|
|_| _||_||_ |_||_   |  | _|
"""

sstr3 = """
    _  _  _  _  _  _  _  _
|_||_   ||_ | ||_|| || || |
  | _|  | _||_||_||_||_||_|
"""

sstr4 = """
 _  _  _  _  _  _     _   
 _| _ |_  _| _| _|     ||_|
|_  _||_ |_ |_||_   |  | _|
"""

print("\nBank Account ")
print("********************************")
s1.fillScreenByStr(sstr2)
print(s1.printNumbers())
print("=> ", s1.readScreenNumbers())