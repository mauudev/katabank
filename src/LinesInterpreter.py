import numpy as np
import itertools
from NumberDictionary import NumberDictionary
from Number import Number

class LinesInterpreter:

  @classmethod
  def buildCellByNumber(self, number):
    cell = []
    sstr = NumberDictionary.numberToString(number)
    cell = self.buildCellByStr(sstr)
    return cell
  
  @classmethod
  def buildCellByStr(self, sstr):
    cell = np.full((3,3), ' ', dtype = 'U1')
    for c, x in zip(sstr, np.nditer(cell, op_flags = ['readwrite'])):
      x[...] = c
    return cell

  @classmethod
  def isEqualToCellNumber(self, number):
    isEqual = False
    pass
    
  @classmethod
  def readNumbersByScreen(self, screen):
    screenIndex, numbers = 0, []
    screenLim = len(screen)
    while screenIndex < screenLim:
      row1 = screen[screenIndex][0]
      row2 = screen[screenIndex][1] 
      row3 = screen[screenIndex][2]
      cellNumber = itertools.chain(row1, row2, row3)
      number = NumberDictionary.getNumberByLines(list(cellNumber))
      if number == None: 
        numbers.append("?")
      else:
        numbers.append(Number(number))
      screenIndex += 1
    return numbers

